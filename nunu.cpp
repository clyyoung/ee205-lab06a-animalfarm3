///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   16 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool isNative, enum Color newColor, enum Gender newGender ) {
   gender = newGender;                 /// Get from the constructor... not all nunu are the same gender (this is a has-a relationship)
   species = "Fistularia chinensis";   /// Hardcode this... all nunu are the same species (this is a is-a relationship)
   scaleColor = newColor;              /// A has-a relationship, so it comes through the constructor
   favoriteTemp = 80.6;                /// An is-a relationship, so it's safe to hardcode.  All nunu have the same favorite temp.
   native = isNative;                  /// A has-a relationship.  Every nunu has its own native status.
}

/// Print our Nunu and native first... then print whatever information Fish holds.
void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << "   Is native = [" << boolalpha << native << "]" << endl;
   Fish::printInfo();
}

} // namespace animalfarm

