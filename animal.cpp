///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   05 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <random>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

   Animal::Animal(void) {cout << '.';};
   Animal::~Animal(void) {cout << 'x';};

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK:  return string("Black"); break;
      case WHITE:  return string("White"); break;
      case RED:    return string("Red"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN:  return string("Brown"); break;
   }
   return string("Unknown");
};

const Gender Animal::getRandomGender() {
   random_device rd;
   int random = rd()%2;
   return Gender(random);
};

const Color Animal::getRandomColor(){
   random_device rd;
   int random = rd()%6;
   return Color(random);
};

const bool Animal::getRandomBool(){
   random_device rd;
   int random = rd()%2;
   bool b;
   switch(random){
      case(0): b = false; break;
      case(1): b = true; break;
   }
   return b;
};

const float Animal::getRandomWeight( const float from, const float to){
   random_device rd;
   mt19937 gen(rd());
   uniform_real_distribution <> dis(from,to);
   return dis(gen);
};

const string Animal::getRandomName(){
   random_device rd;
   int random = (rd()%(9-4+1))+4;
   string randomName;
   randomName += (char)(rd()%(90-65+1))+65;
   for(int i = 1; i < random ; i++){
      randomName += (char)(rd()%(122-97+1))+97;
   }
   return randomName;
};

} // namespace animalfarm
