///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   05 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <random>
#include <array>
#include <list>

#include "animal.hpp"
#include "factory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 3" << endl;

   // Create and fill animalArray with 25 random animals (constructor)
   array<Animal*, 30> animalArray;
   animalArray.fill(NULL);
   for(int i = 0; i < 25; i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }

   // Print facts about animalArray
   cout << endl << "Array of Animals" << endl;
   cout << "  Is it empty: " << boolalpha << animalArray.empty() << endl;
   cout << "  Number of elements: " << animalArray.size() << endl;
   cout << "  Max size: " << animalArray.max_size() << endl;

   // Make each animal in animalArray speak
   for(Animal* animal : animalArray){
      if(animal != NULL){
      cout << animal->speak() << endl;
      }
   }
   
   // Remove each animal from animalArray (destructor)
   for(Animal* animal : animalArray){
      if(animal != NULL){
         delete animal;
      }
   }

   // Create and fill animalList with 25 random animals (constructor)
   list<Animal*> animalList;
   for(int i = 0; i < 25; i++){
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }

   // Print facts about animalList
   cout << endl << "List of Animals" << endl;
   cout << "  Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "  Number of elements: " << animalList.size() << endl;
   cout << "  Max size: " << animalList.max_size() << endl;

   // Make each animal in animalList speak
   for(Animal* animal : animalList){
      cout << animal->speak() << endl;
   }

   // Remove each animal from animalList (destructor)
   for(Animal* animal : animalList){
      delete animal;
   }
   return 0;
}
